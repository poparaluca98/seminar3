let generateCheckPrime=()=>{
	let cache=[2,3]
	let checkAgainstCache=(n)=>{
		for(let e of cache){
			if(!(n%e)){
				return false
			}
		}
		return true
	}
	return(n)=>{
		if(n<=cache[cache.length-1]){
			if(cache.indexOf(n)!==-1){
				return true
			}
			else{
				return false
			}
		}
		else
		{
			for(let i=cache[cache.length-1]+2;i<=Math.sqrt(n);i+=2){
				if(checkAgainstCache(i)){
					cache.push(i);
				}
			}
			console.log(cache)
			return checkAgainstCache(n)
		}
	}
}
let checkPrime=generateCheckPrime()
console.log(checkPrime(121))
console.log(checkPrime(173))
console.log(checkPrime(37))
console.log(checkPrime(1023))

