'use strict'
class Robot{
	constructor (name)
	{
		this.name=name;
	}

		move(){
			console.log(`${this.name} is moving`)
		}
	
}

let r0=new Robot('some robot')
r0.move()

class Weapon{
	constructor(description){
		this.description=description
	}
	fire(){
		console.log(`${this.description} is firing`)
	}
}
let w0=new Weapon('pew pew laser')
w0.fire()

class CombatRobot extends Robot{
	constructor(name)
	{
		super(name)
		this.weapons=[]
	}
	addWeapon(w){
		this.weapons.push(w)
	}
	fire()
	{
		console.log('firing all weapons')
		for(let w of this.weapons){
			w.fire();
		}
	}
}

let r1=new CombatRobot('some combat robot')
r1.move()
r1.addWeapon(w0)
r1.fire()

Robot.prototype.fly=function(){
	console.log(`${this.name} is flaying`)
}
r1.fly()
let f0=r1.fly
//f0()
f0.apply(r1,)
f0.call(r1)
