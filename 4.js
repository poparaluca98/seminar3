let getTmedFunction=(f)=>{
return(...args)=>{
	let before=Date.now()
	let r= f(...args)

	 console.log(`${f.name} ran in ${Date.now() - before} milliseconds`)
	 return r

}
}
let sampleF=(a,b,c)=>{
	for(let i=0;i<10000;i++){
		console.log(a+b+c)
	}
}
let timedSampleF=getTmedFunction(sampleF)
timedSampleF(1,2,3)